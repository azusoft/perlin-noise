#Basic file

require 'gosu'
require 'perlin_noise'
load 'async-noise.rb'

WINDOW_HEIGHT = 320
WINDOW_WIDTH = 568

class Window < Gosu::Window
  def initialize
    super WINDOW_WIDTH, WINDOW_HEIGHT, false, 1
    self.caption = "Perlin noise"
    @noise_getter = Noise.new(2)
    @offset = 0
  end

  def update
    @offset += 0.01
  end

  def draw
    (0..WINDOW_WIDTH).each do |w|
      (0..WINDOW_HEIGHT).each do |h|
        @noise_getter.await.draw_pixel(w, h, @offset, @offset)
      end
    end
  end
end

window = Window.new
window.show