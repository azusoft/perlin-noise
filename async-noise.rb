require 'concurrent'

class Noise
  include Concurrent::Async

  def initialize(dimension = 1)
    @noises = Perlin::Noise.new(dimension)
    @contrast = Perlin::Curve.contrast(Perlin::Curve::CUBIC, dimension)
  end

  def draw_pixel(x = 0, y = 0, x_off = 0, y_off = 0)
    color = (@contrast.call @noises[x_off + x * 0.01, y_off + y * 0.01]) * 255
    Gosu.draw_rect(x, y, 1, 1, Gosu::Color.new(color, 255, 255, 255))
  end
end